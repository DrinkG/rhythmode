create table user(
id_user int not null auto_increment primary key,
name varchar(50),
lastname varchar(50),
username varchar(50),
email varchar(255),
password varchar(60),
verified boolean default 0,
is_admin boolean default 0,
is_verified_check boolean default 0,
created_at datetime,
CONSTRAINT UQ_USERNAME UNIQUE (username),
CONSTRAINT UQ_EMAIL UNIQUE (email)
);

create table pt_type_privacy(
id_type_privacy int not null auto_increment primary key,
description varchar(100),
CONSTRAINT UQ_DESCRIPTION UNIQUE (description)
);

create table pt_country(
id_country int not null auto_increment primary key,
name varchar(50),
prefix varchar(10),
CONSTRAINT UQ_COUNTRY UNIQUE (name,prefix)
);

create table profile(
id_profile int not null auto_increment primary key,
image_header varchar(255),
day_of_birth date ,
title varchar(255),
bio varchar(255),
address varchar(255),
phone varchar(12),
type_privacy_id int,
user_id int,
country_id int,
foreign key (type_privacy_id) references pt_type_privacy(id_type_privacy),
foreign key (country_id) references pt_country(id_country),
foreign key (user_id) references user(id_user)
);

create table friend(
id_friend int not null auto_increment primary key,
user_from_id int,
user_to_id int,
created_at datetime,
CONSTRAINT UQ_from_to UNIQUE (user_from_id,user_to_id),
foreign key (user_from_id) references user(id_user),
foreign key (user_to_id) references user(id_user)
);

create table post(
id_post int not null auto_increment primary key,
title varchar(200),
location varchar(500),
src varchar(255) not null,
user_id int,
created_at datetime,
foreign key (user_id) references user(id_user)
);

create table comment_post(
id_comment_post int not null auto_increment primary key,
content varchar(255),
created_at datetime,
references_to int default 0,
user_id int,
post_id int,
foreign key (post_id) references post(id_post),
foreign key (user_id) references user(id_user)
);

create table like_post(
id_like_post int not null auto_increment primary key,
liked boolean default 0,
user_id int,
post_id int,
created_at datetime,
foreign key (post_id) references post(id_post),
foreign key (user_id) references user(id_user)
);