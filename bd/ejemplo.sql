INSERT INTO pt_type_privacy(description)
VALUES
('private'),('public'),('friends');


INSERT INTO `rhythmode`.`user`(`name`,`lastname`,`username`,`email`,`password`,`verified`,`is_admin`,`is_verified_check`,`created_at`)
VALUES
('Pepe1','Sanchez1','PepeSan1','pepesam1@asd.es','asd1',0,0,0,'2021-05-30 17:00:53'),
('Pepe2','Sanchez2','PepeSan2','pepesam2@asd.es','asd2',1,0,0,'2021-05-30 17:00:53'),
('Pepe3','Sanchez3','PepeSan3','pepesam3@asd.es','asd3',2,0,0,'2021-05-30 17:00:53'),
('Pepe4','Sanchez4','PepeSan4','pepesam4@asd.es','asd4',3,0,0,'2021-05-30 17:00:53'),
('Pepe5','Sanchez5','PepeSan5','pepesam5@asd.es','asd5',4,0,0,'2021-05-30 17:00:53');



INSERT INTO `rhythmode`.`friend`(`user_from_id`,`user_to_id`,`created_at`)
VALUES
(1,2,'2021-05-30 17:19:53'),
(1,3,'2021-05-30 17:19:53'),
(1,4,'2021-05-30 17:19:53'),
(1,5,'2021-05-30 17:19:53'),
(2,3,'2021-05-30 17:19:53'),
(2,4,'2021-05-30 17:19:53'),
(2,5,'2021-05-30 17:19:53'),
(3,4,'2021-05-30 17:19:53'),
(3,5,'2021-05-30 17:19:53'),
(4,5,'2021-05-30 17:19:53');



INSERT INTO `rhythmode`.`profile`(`image_header`,`day_of_birth`,`title`,`bio`,`address`,`phone`,`type_privacy_id`,`user_id`,`country_id`)
VALUES
('asd1.com',null,'guapo','guapo','guapo',null,1,1,4),
('asd2.com',null,'guapo1','guapo1','guapo1',null,null,2,5),
('asd3.com',null,'guapo2','guapo2','guapo2',null,1,3,6),
('asd4.com',null,'guapo3','guapo3','guapo3',null,2,4,7),
('asd5.com',null,'guapo4','guapo4','guapo4',null,1,5,8);



INSERT INTO `rhythmode`.`post`(`title`,`location`,`src`,`user_id`,`created_at`)
VALUES
('FOTON_1','Zaragoza','ppp.es',1,'2021-05-30 20:44:00'),
('FOTON_2','Zaragoza','ppp.es',2,'2021-05-30 20:44:00'),
('FOTON_3','Zaragoza','ppp.es',3,'2021-05-30 20:44:00'),
('FOTON_4','Zaragoza','ppp.es',1,'2021-05-30 20:44:00'),
('FOTON_5','Zaragoza','ppp.es',1,'2021-05-30 20:44:00'),
('FOTON_6','Zaragoza','ppp.es',2,'2021-05-30 20:44:00');



INSERT INTO `rhythmode`.`like_post`(`liked`,`user_id`,`post_id`,`created_at`)
VALUES
(-1,1,1,'2021-05-31 19:37:21'),
(1,2,1,'2021-05-31 19:37:21'),
(-1,3,3,'2021-05-31 19:37:21'),
(1,4,2,'2021-05-31 19:37:21'),
(1,5,3,'2021-05-31 19:37:21');



INSERT INTO `rhythmode`.`comment_post`(`content`,`created_at`,`references_to`,`user_id`,`post_id`)
VALUES
('texto1','2021-05-30 14:00:17',0,1,1),
('texto2','2021-05-30 14:00:17',1,1,1),
('texto3','2021-05-30 14:00:19',4,1,1),
('texto4','2021-05-30 14:00:19',0,3,1),
('texto5','2021-05-30 14:00:21',5,1,1),
('texto6','2021-05-30 14:00:21',0,1,1);