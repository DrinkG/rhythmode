import { Injectable } from '@angular/core';
import { HttpClient, HttpClientModule } from '@angular/common/http';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class SigninUpService {

  constructor(private router: Router,private http: HttpClient) { }

  signUpUser(name1, lastname1, username1, email1, password1) {
    console.log(name1, lastname1, username1, email1, password1);
    const headers = {};
    const body = { name1, lastname1, username1, email1, password1 };
    this.http.post<any>('http://rhythmode-env.eba-mthpe3u4.eu-west-3.elasticbeanstalk.com/user/insert', body, { headers }).subscribe(data => {
      console.log(data);
      this.router.navigate(['/user-panel']);
    });

  }
}
