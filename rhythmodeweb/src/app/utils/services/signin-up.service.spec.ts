import { TestBed } from '@angular/core/testing';

import { SigninUpService } from './signin-up.service';

describe('SigninUpService', () => {
  let service: SigninUpService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(SigninUpService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
