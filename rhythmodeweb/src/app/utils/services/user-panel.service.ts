import { Injectable } from '@angular/core';
import { ReplaySubject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class UserPanelService {
  sharedStyleSource = new ReplaySubject<any>(1);
  public sharedStyle$ = this.sharedStyleSource.asObservable();
  constructor() { }

  newStyle(value: any) {
    console.log(value)
    this.sharedStyleSource.next(value);
  }

}
