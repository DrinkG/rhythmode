export class Texts{
    HERO1: TextModel = {
        title : "Escucha las últimas canciones",
        body : "Enterate de las últimas canciones subidas por nuestros artistas en la sección de explora y mediante un sistema de votos, decide si te gustan o no. No importa el género, ni la calidad, ni el artista, en RhythMode solo queremos que vosotros disfruteis de la música, conozcais artistas nuevos y tengais la oportunidad de formar parte de la industria musical.",
        button_text : "Explorar",
        link: "/explore"
    }

    HERO2: TextModel = {
        title : "Busca en destacados",
        body : "Descubre las canciones mejores valoradas de la web en la sección de destacados. Para entrar dentro solo tienes que conseguir que a la gente le guste tu música! Sube tu videoclip o canción ahora mismo!",
        button_text : "Destacados",
        link: "/feature"
    }

    constructor() { }
}

class TextModel {
    title: string;
    body: string;
    button_text: string;
    link: string;
}