import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';

@Component({
  selector: 'app-sign-in',
  templateUrl: './sign-in.component.html',
  styleUrls: ['./sign-in.component.scss']
})
export class SignInComponent implements OnInit {
  signInForm : FormGroup;

  emailPattern: "^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$";

  constructor(private _formBuilder: FormBuilder, private router: Router) { }

  ngOnInit(): void {
    this.signInForm = this._formBuilder.group({
      email: ['', Validators.required],
      password: ['', Validators.required]
    });
  }

  submitSignInForm(){
    if (this.email.value == "" || this.password.value == "") {
      if(this.email.value == ""){
        this.email.markAsDirty();
      }
      if(this.password.value == ""){
        this.password.markAsDirty();
      }
    }
    else{
      console.log(this.email.value);
      console.log(this.password.value);
      this.router.navigate(['/user-panel']);
    }
  }

  get email() { return this.signInForm.get('email'); }
  get password() { return this.signInForm.get('password'); }

}
