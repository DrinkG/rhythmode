import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';

@Component({
  selector: 'app-sign-up',
  templateUrl: './sign-up.component.html',
  styleUrls: ['./sign-up.component.scss']
})
export class SignUpComponent implements OnInit {
  signInForm : FormGroup;

  emailPattern: "^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$";

  constructor(private router: Router,private _formBuilder: FormBuilder) { }

  ngOnInit(): void {
    this.signInForm = this._formBuilder.group({
      email: ['', Validators.required],
      password: ['', Validators.required],
      name: ['', Validators.required],
      surname: ['', Validators.required],
      address: ['', Validators.required],
      tlfn: ['', Validators.required],
    });
  }

  submitSignUpForm(){
    if (this.email.value == "" || this.password.value == "") {
        this.signInForm.markAsDirty();
    }
    else{
      this.router.navigate(['/register-successful']);
    }
  }

  get email() { return this.signInForm.get('email'); }
  get password() { return this.signInForm.get('password'); }
  get name() { return this.signInForm.get('name'); }
  get surname() { return this.signInForm.get('surname'); }
  get address() { return this.signInForm.get('address'); }
  get tlfn() { return this.signInForm.get('tlfn'); }

}
