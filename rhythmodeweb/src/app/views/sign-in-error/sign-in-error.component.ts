import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-sign-in-error',
  templateUrl: './sign-in-error.component.html',
  styleUrls: ['./sign-in-error.component.scss']
})
export class SignInErrorComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}
