import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ScrollableInfoComponent } from './scrollable-info.component';

describe('ScrollableInfoComponent', () => {
  let component: ScrollableInfoComponent;
  let fixture: ComponentFixture<ScrollableInfoComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ScrollableInfoComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ScrollableInfoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
