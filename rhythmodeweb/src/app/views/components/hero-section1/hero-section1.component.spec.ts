import { ComponentFixture, TestBed } from '@angular/core/testing';

import { HeroSection1Component } from './hero-section1.component';

describe('HeroSection1Component', () => {
  let component: HeroSection1Component;
  let fixture: ComponentFixture<HeroSection1Component>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ HeroSection1Component ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(HeroSection1Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
