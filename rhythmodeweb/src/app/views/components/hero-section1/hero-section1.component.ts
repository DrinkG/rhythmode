import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-hero-section1',
  templateUrl: './hero-section1.component.html',
  styleUrls: ['./hero-section1.component.scss']
})
export class HeroSection1Component implements OnInit {

  @Input()
  hero1: any;

  constructor() { }

  ngOnInit(): void {
  }

}
