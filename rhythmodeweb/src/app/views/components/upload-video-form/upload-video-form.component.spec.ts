import { ComponentFixture, TestBed } from '@angular/core/testing';

import { UploadVideoFormComponent } from './upload-video-form.component';

describe('UploadVideoFormComponent', () => {
  let component: UploadVideoFormComponent;
  let fixture: ComponentFixture<UploadVideoFormComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ UploadVideoFormComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(UploadVideoFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
