import { Component, OnInit } from '@angular/core';
import { UserPanelService } from '../../../utils/services/user-panel.service';
@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.scss']
})
export class SidebarComponent implements OnInit {
  links = [
    {
      name: "Inicio",
      url: ""
    },
    {
      name: "Lista",
      url: "list"
    },
    {
      name: "Formulario",
      url: "form"
    }
  ]

  constructor(private upService : UserPanelService) { }

  ngOnInit(): void {
  }

  loadInfoPanel(){
    this.upService.newStyle({ 'display': 'inline-block' });

  }
}
