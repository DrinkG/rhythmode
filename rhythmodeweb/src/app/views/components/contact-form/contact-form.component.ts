import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-contact-form',
  templateUrl: './contact-form.component.html',
  styleUrls: ['./contact-form.component.scss']
})

export class ContactFormComponent implements OnInit {
  emailPattern = "[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?";
  noNumbersPattern = "[a-zA-Z]+[-_]*[a-zA-Z]+";
  form : FormGroup;
  panelOpenState = false;
  has_accepted;
  form_validated : boolean;
  checkBoxValidation : boolean;

  constructor(private fb : FormBuilder) { }

  ngOnInit(): void {

    this.form = this.fb.group({
      name: ['', Validators.required],
      email: ['', Validators.required],
      message: ['', Validators.required]
    });

  }

    //checks if privacy policy is checked or unchecked.
    checkCheckBoxvalue(event) {
      this.has_accepted = event.checked;
    }

  submitContactForm() {
    if ( this.name.value == "" || this.email.value == "" ){
      this.form.markAsDirty();
    }
    else{
      if( this.has_accepted == false) {
        this.checkBoxValidation = true;
      }else{
        this.checkBoxValidation = false;
        //this.sendEmail();
      }
    }
  }

  checkBoxvalue(event) {
    this.has_accepted = event.checked;
  }
  //CAMBIAR SCRIPT GOOGLE

  // sendEmail() {
  //   const headers = new HttpHeaders({
  //     'Content-Type' : 'application/x-www-form-urlencoded'
  //   });

  //   const options = { headers: headers };

  //   let body = JSON.stringify(this.form.value);
    

  //   this.http.post<any>(`https://script.google.com/macros/s/AKfycbySXk2kEc6g_pCq-h8y5ZRmzTDo_go2GwPkoxT_PpOyj0ZyxZts/exec`, body, options)
  //     .subscribe(res => {
  //       console.log('res', res)
  //       if (res.result == "success"){
  //         const dialogRef = this.dialog.open(ContactFormResponseComponent, {
  //           width: '100%',
  //         });
  //         dialogRef.afterClosed().subscribe(result => {
  //             location.reload()
  //         });
  //       }else{

  //       }
  //     })
  // }

  get name() { return this.form.get('name'); }
  get email() { return this.form.get('email'); }
  get message() { return this.form.get('message'); }

}
