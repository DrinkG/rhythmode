import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes} from '@angular/router';

import { PrivacyPolicyComponent } from './components/privacy-policy/privacy-policy.component';
import { TermsOfUseComponent } from './components/terms-of-use/terms-of-use.component';
import { HomePageComponent } from './home-page/home-page.component';

import { MatExpansionModule } from '@angular/material/expansion';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatButtonModule } from '@angular/material/button';
import { MatIconModule } from '@angular/material/icon';
import { MatDividerModule } from '@angular/material/divider';
import { MatList, MatListModule } from '@angular/material/list';
import {FormsModule, ReactiveFormsModule} from '@angular/forms'

import { ContactFormComponent } from './components/contact-form/contact-form.component';
import { SignInComponent } from './sign-in/sign-in.component';
import { SignInSuccessfullComponent } from './sign-in-successfull/sign-in-successfull.component';
import { SignInErrorComponent } from './sign-in-error/sign-in-error.component';
import { SignUpComponent } from './sign-up/sign-up.component';
import { UserPanelComponent } from './user-panel/user-panel.component';
import { HeroSection1Component } from './components/hero-section1/hero-section1.component';
import { LandingVideoComponent } from './components/landing-video/landing-video.component';
import { UploadVideoFormComponent } from './components/upload-video-form/upload-video-form.component';
import { ScrollableInfoComponent } from './components/scrollable-info/scrollable-info.component';
import { FeatureComponent } from './feature/feature.component';
import { ExploreComponent } from './explore/explore.component';
import { SidebarComponent } from './components/sidebar/sidebar.component';
import { CardsComponent } from './components/cards/cards.component';
import { UserInfoComponent } from './components/user-info/user-info.component';
import {SwipeCardLibModule} from 'ng-swipe-card';
import { SignUpSuccessfulComponent } from './sign-up-successful/sign-up-successful.component';
import { UploadSongComponent } from './upload-song/upload-song.component';


const routes : Routes = [
  {
    path: '',
    component: HomePageComponent
  },
  {
    path: 'sign-in',
    component: SignInComponent
  },
  {
    path: 'sign-up',
    component: SignUpComponent
  },
  {
    path: 'explore',
    component: ExploreComponent
  },
  {
    path: 'feature',
    component: FeatureComponent
  },
  {
    path: 'user-panel',
    component: UserPanelComponent
  },
  {
    path: 'privacy-policy',
    component: PrivacyPolicyComponent
  },
  {
    path: 'terminos-condiciones',
    component: TermsOfUseComponent
  },
  {
    path: 'register-successful',
    component: SignUpSuccessfulComponent
  },
  {
    path: 'upload-song',
    component: UploadSongComponent
  }
]

@NgModule({
  declarations: [
    HomePageComponent,
    PrivacyPolicyComponent,
    TermsOfUseComponent,
    ContactFormComponent,
    SignInComponent,
    SignInSuccessfullComponent,
    SignInErrorComponent,
    SignUpComponent,
    UserPanelComponent,
    HeroSection1Component,
    LandingVideoComponent,
    UploadVideoFormComponent,
    ScrollableInfoComponent,
    FeatureComponent,
    ExploreComponent,
    SidebarComponent,
    CardsComponent,
    UserInfoComponent,
    SignUpSuccessfulComponent,
    UploadSongComponent
  ],
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    MatCheckboxModule,
    MatExpansionModule,
    ReactiveFormsModule,
    FormsModule,
    MatToolbarModule,
    MatSidenavModule,
    MatButtonModule,
    MatIconModule,
    MatDividerModule,
    MatListModule,
    SwipeCardLibModule 
  ]
})
export class ViewsModule { }
