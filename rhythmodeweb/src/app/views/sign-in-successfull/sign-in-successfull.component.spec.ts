import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SignInSuccessfullComponent } from './sign-in-successfull.component';

describe('SignInSuccessfullComponent', () => {
  let component: SignInSuccessfullComponent;
  let fixture: ComponentFixture<SignInSuccessfullComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SignInSuccessfullComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SignInSuccessfullComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
