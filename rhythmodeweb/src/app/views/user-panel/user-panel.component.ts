import { Component, OnInit } from '@angular/core';
import { UserPanelService } from '../../utils/services/user-panel.service';
@Component({
  selector: 'app-user-panel',
  templateUrl: './user-panel.component.html',
  styleUrls: ['./user-panel.component.scss']
})
export class UserPanelComponent implements OnInit {

  constructor(public upService: UserPanelService ) { }

  ngOnInit(): void {
  }

}
