import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-explore',
  templateUrl: './explore.component.html',
  styleUrls: ['./explore.component.scss']
})
export class ExploreComponent implements OnInit {
  src: any[] = [
    "https://www.youtube.com/embed/nSD0dKeOra0",
    "https://www.youtube.com/embed/OIXwDqXKEUo",
    "https://www.youtube.com/embed/-h8Q6kJoScg",
    "https://www.youtube.com/embed/EVzHTECkDDA",
    "https://www.youtube.com/embed/7Q1luOdSCKY",
    "https://www.youtube.com/embed/GoN7N9FnH1c",
    "https://www.youtube.com/embed/oLFH2dzI6GQ",
    "https://www.youtube.com/embed/mWWD5pYRjD4",
    "https://www.youtube.com/embed/HaYhQCjL8r4",
    "https://www.youtube.com/embed/9BG5ZE8ilc0",
  ];
  checker: boolean = false;
  contador: any[] = [];
  constructor() { }

  ngOnInit(): void {
    this.contador = [];
    this.changeVideo();
  }

  changeVideo() {
    var a = this.getRandomInt(0, this.src.length)
    var iframe = document.getElementById('video').setAttribute('src', this.src[a]);
  }

  getRandomInt(min, max) {
    console.log(this.contador)
    var b = Math.floor(Math.random() * (max - min)) + min;
    do {
      console.log(this.checker);
      this.checker = false;
      for (let i = 0; i < this.contador.length; i++) {
        if(this.contador.length > 0){
        if (b == this.contador[i]) {
          this.checker = true;

        }}
      }
      if (this.checker == true) {
        var b = Math.floor(Math.random() * (max - min)) + min;
      }
      if( this.contador.length == this.src.length){
        this.checker = false;
      }
    } while(this.checker == true);
    if( this.contador.length == this.src.length){
      return null;
    }else{
    this.contador.push(b);
    return b;
  }
  }
}
