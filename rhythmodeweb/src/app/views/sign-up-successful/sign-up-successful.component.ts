import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-sign-up-successful',
  templateUrl: './sign-up-successful.component.html',
  styleUrls: ['./sign-up-successful.component.scss']
})
export class SignUpSuccessfulComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}
