import { Component, OnInit } from '@angular/core';
import { Texts } from 'src/app/utils/texts';

@Component({
  selector: 'app-home-page',
  templateUrl: './home-page.component.html',
  styleUrls: ['./home-page.component.scss']
})
export class HomePageComponent implements OnInit {

  text: Texts = new Texts();
  hero1: any;  
  hero2: any;  

  constructor() { }

  ngOnInit(): void {
    this.hero1 = this.text.HERO1;
    this.hero2 = this.text.HERO2;
  }

}
