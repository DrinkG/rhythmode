package com.is.rhythmode.config;

public class Constants {
    public static final String SECRET = "vTfK1UdBfu$#DCy";
    public static final long EXPIRATION_TIME = 900000000;
    public static final String HEADER = "Authorization";
    public static final String PREFIX = "Bearer ";
    public static final String LOGIN_URL = "/login";
    public static final String REGISTER_URL = "/register";
}