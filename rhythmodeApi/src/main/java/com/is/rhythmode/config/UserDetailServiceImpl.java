package com.is.rhythmode.config;

import com.is.rhythmode.model.entity.UserEntity;
import com.is.rhythmode.model.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.Collections;
import java.util.Optional;

@Service
public class UserDetailServiceImpl implements UserDetailsService {

    @Autowired
    private UserRepository userRepository;

    @Override
    public UserDetails loadUserByUsername(String email) throws UsernameNotFoundException {
        UserEntity user;
        Optional<UserEntity> userOptional = userRepository.findByEmail(email);

        if(!userOptional.isPresent()){
            throw new UsernameNotFoundException(email);
        }else{
            user = userOptional.get();
        }

        return new User(user.getEmail(), user.getPassword(), Collections.emptyList());
    }
}
