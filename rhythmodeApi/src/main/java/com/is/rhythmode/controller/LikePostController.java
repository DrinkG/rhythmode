package com.is.rhythmode.controller;

import com.is.rhythmode.exception.RhythModeException;
import com.is.rhythmode.model.entity.LikePost;
import com.is.rhythmode.service.LikePostService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(value = "/likePost")
public class LikePostController {
    @Autowired
    private LikePostService likePostService;

    @GetMapping()
    private ResponseEntity<List<LikePost>> getAllLikePost() {
        try {
            List<LikePost> likePostList = likePostService.searchLikePostList();
            return new ResponseEntity<>(likePostList, HttpStatus.OK);
        } catch (RhythModeException e) {
            return new ResponseEntity(e.getMessage(), HttpStatus.NOT_FOUND);
        }
    }

    @GetMapping(value = "/{idLikePost}")
    private ResponseEntity<LikePost> getLikePostById(@PathVariable Long idLikePost) {
        try {
            LikePost likePost=likePostService.getById(idLikePost);
            return new ResponseEntity<LikePost>(likePost, HttpStatus.OK);
        } catch (RhythModeException e) {
            return new ResponseEntity(null, HttpStatus.NOT_FOUND);
        }
    }

    @GetMapping(value = "/likeOfPost/{idPost}")
    private ResponseEntity<List<LikePost>> searchLikePostByIdPost(@PathVariable Long idPost) {
        try {
            List<LikePost> likePostList = likePostService.searchLikePostByIdPost(idPost);
            return new ResponseEntity<>(likePostList, HttpStatus.OK);
        } catch (RhythModeException e) {
            return new ResponseEntity(e.getMessage(), HttpStatus.NOT_FOUND);
        }
    }

    @GetMapping(value = "/countLikePost/{idPost}")
    private ResponseEntity<Long> getCountLikePost(@PathVariable Long idPost) {
        try {
            Long likePostCount = likePostService.countLikesByIdPost(idPost);
            return new ResponseEntity<>(likePostCount, HttpStatus.OK);
        } catch (RhythModeException e) {
            return new ResponseEntity(e.getMessage(), HttpStatus.NOT_FOUND);
        }
    }

    @PostMapping(value = "/add")
    private ResponseEntity<String> createLikePost(@RequestParam(value = "liked", required = true) int liked,
                                              @RequestParam(value = "idUser", required = true) Long idUser,
                                              @RequestParam(value = "idPost", required = true) Long idPost
    ) {
        try {
            likePostService.createLikePost(idUser, idPost, liked);

            return new ResponseEntity<>("Like dado satisfactoriamente", HttpStatus.OK);
        } catch (RhythModeException e) {
            return new ResponseEntity(e.getMessage(), HttpStatus.OK);
        }
    }

    @DeleteMapping(value = "/delete")
    private ResponseEntity<Boolean> deleteLikePostById(@RequestParam(value="idLikePost", required=true) Long idLikePost) {
        try{
            likePostService.deleteLikePost(idLikePost);
            return new ResponseEntity<Boolean>(true, HttpStatus.OK);
        } catch (RhythModeException e) {
            return new ResponseEntity<Boolean>(false, HttpStatus.OK);
        }
    }
}