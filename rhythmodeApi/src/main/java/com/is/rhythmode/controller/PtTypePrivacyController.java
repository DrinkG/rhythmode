package com.is.rhythmode.controller;

import com.is.rhythmode.exception.RhythModeException;
import com.is.rhythmode.model.entity.PtTypePrivacy;
import com.is.rhythmode.service.PtTypePrivacyService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(value = "/typePrivacy")
public class PtTypePrivacyController {
    @Autowired
    private PtTypePrivacyService ptTypePrivacyService;

    @GetMapping()
    private ResponseEntity<List<PtTypePrivacy>> getAllTypePrivacy() {
        try {
            List<PtTypePrivacy> ptCountryList = ptTypePrivacyService.searchTypePrivacities();
            return new ResponseEntity<>(ptCountryList, HttpStatus.OK);
        } catch (RhythModeException e) {
            return new ResponseEntity(e.getMessage(), HttpStatus.NOT_FOUND);
        }
    }

    @GetMapping(value = "/{idPtTypePrivacy}")
    private ResponseEntity<PtTypePrivacy> getPtTypePrivacy(@PathVariable Long idPtTypePrivacy) {
        try {
            PtTypePrivacy ptTypePrivacy = ptTypePrivacyService.getById(idPtTypePrivacy);
            return new ResponseEntity<PtTypePrivacy>(ptTypePrivacy, HttpStatus.OK);
        } catch (RhythModeException e) {
            return new ResponseEntity(null, HttpStatus.NOT_FOUND);
        }
    }

    @DeleteMapping(value = "/delete")
    private ResponseEntity<Boolean> deletePtTypePrivacyControllerById(@RequestParam(value="idPtTypePrivacy", required=true) Long idPtTypePrivacy) {
        try{
            ptTypePrivacyService.deletePtTypePrivacity(idPtTypePrivacy);
            return new ResponseEntity<Boolean>(true, HttpStatus.OK);
        } catch (RhythModeException e) {
            return new ResponseEntity<Boolean>(false, HttpStatus.OK);
        }
    }
}