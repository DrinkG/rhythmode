package com.is.rhythmode.controller;

import com.is.rhythmode.exception.RhythModeException;
import com.is.rhythmode.model.entity.Friend;
import com.is.rhythmode.service.FriendService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(value = "/friend")
public class FriendController {
    @Autowired
    private FriendService friendService;

    @GetMapping()
    private ResponseEntity<List<Friend>> getAllFriends() {
        try {
            List<Friend> commentPostList = friendService.searchFriends();
            return new ResponseEntity<>(commentPostList, HttpStatus.OK);
        } catch (RhythModeException e) {
            return new ResponseEntity(e.getMessage(), HttpStatus.NOT_FOUND);
        }
    }

    @GetMapping(value = "/{idFriend}")
    private ResponseEntity<Friend> getFriendById(@PathVariable Long idFriend) {
        try {
            Friend commentPost=friendService.getById(idFriend);
            return new ResponseEntity<Friend>(commentPost, HttpStatus.OK);
        } catch (RhythModeException e) {
            return new ResponseEntity(null, HttpStatus.NOT_FOUND);
        }
    }

    @GetMapping(value = "/followFrom/{idUser}")
    private ResponseEntity<List<Friend>> getFollowsFromByIdUser(@PathVariable Long idUser) {
        try {
            List<Friend> commentPostList = friendService.findFollowFromByUser(idUser);
            return new ResponseEntity<>(commentPostList, HttpStatus.OK);
        } catch (RhythModeException e) {
            return new ResponseEntity(e.getMessage(), HttpStatus.NOT_FOUND);
        }
    }

    @GetMapping(value = "/followTo/{idUser}")
    private ResponseEntity<List<Friend>> getFollowsToByIdUser(@PathVariable Long idUser) {
        try {
            List<Friend> commentPostList = friendService.findFollowToByUser(idUser);
            return new ResponseEntity<>(commentPostList, HttpStatus.OK);
        } catch (RhythModeException e) {
            return new ResponseEntity(e.getMessage(), HttpStatus.NOT_FOUND);
        }
    }

    @GetMapping(value = "/followerCount/{idUser}")
    private ResponseEntity<Long> getFollowersCount(@PathVariable Long idUser) {
        Long commentPostList = friendService.followerCountByUser(idUser);
        return new ResponseEntity<>(commentPostList, HttpStatus.OK);
    }

    @GetMapping(value = "/followedCount/{idUser}")
    private ResponseEntity<Long> getFollowedCount(@PathVariable Long idUser) {
        Long commentPostList = friendService.followedCountByUser(idUser);
        return new ResponseEntity<>(commentPostList, HttpStatus.OK);
    }

    @PostMapping(value = "/add")
    private ResponseEntity<String> createFriend(@RequestParam(value = "idUserFrom", required = true) Long idUserFrom,
                                              @RequestParam(value = "idUserTo", required = true) Long idUserTo
    ) {
        try {
            friendService.addFriend(idUserFrom, idUserTo);

            return new ResponseEntity<>("Amigo añadido satisfactoriamente", HttpStatus.OK);
        } catch (RhythModeException e) {
            return new ResponseEntity(e.getMessage(), HttpStatus.OK);
        }
    }

    @DeleteMapping(value = "/delete")
    private ResponseEntity<Boolean> deleteFriendById(@RequestParam(value="idFriend", required=true) Long idFriend) {
        try{
            friendService.deleteFriend(idFriend);
            return new ResponseEntity<Boolean>(true, HttpStatus.OK);
        } catch (RhythModeException e) {
            return new ResponseEntity<Boolean>(false, HttpStatus.OK);
        }
    }
}

