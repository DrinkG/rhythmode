package com.is.rhythmode.controller;

import com.is.rhythmode.exception.RhythModeException;
import com.is.rhythmode.model.entity.CommentPost;
import com.is.rhythmode.service.CommentPostService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(value = "/commentPost")
public class CommentPostController {
    @Autowired
    private CommentPostService commentPostService;

    @GetMapping()
    private ResponseEntity<List<CommentPost>> getAllCommentPost() {
        try {
            List<CommentPost> commentPostList = commentPostService.searchCommentPost();
            return new ResponseEntity<>(commentPostList, HttpStatus.OK);
        } catch (RhythModeException e) {
            return new ResponseEntity(e.getMessage(), HttpStatus.NOT_FOUND);
        }
    }

    @GetMapping(value = "/{idCommentPost}")
    private ResponseEntity<CommentPost> getCommentPostByIdPost(@PathVariable Long idCommentPost) {
        try {
            CommentPost commentPost=commentPostService.getById(idCommentPost);
            return new ResponseEntity<CommentPost>(commentPost, HttpStatus.OK);
        } catch (RhythModeException e) {
            return new ResponseEntity(null, HttpStatus.NOT_FOUND);
        }
    }

    @PostMapping(value = "/add")
    private ResponseEntity<String> createCommentPost(@RequestParam(value = "idUser", required = true) Long idUser,
                                                  @RequestParam(value = "idPost", required = true) Long idPost,
                                                  @RequestParam(value = "content", required = true) String content,
                                                  @RequestParam(value = "referencesTo", required = false) Integer referencesTo
    ) {
        try {
            commentPostService.createCommentPost(idUser, idPost, content,referencesTo);

            return new ResponseEntity<>("Comentario creado satisfactoriamente", HttpStatus.OK);
        } catch (RhythModeException e) {
            return new ResponseEntity(e.getMessage(), HttpStatus.OK);
        }
    }

    @DeleteMapping(value = "/delete")
    private ResponseEntity<Boolean> deleteCommentPostById(@RequestParam(value="idCommentPost", required=true) Long idCommentPost) {
        try {
            commentPostService.deleteCommentPost(idCommentPost);
            return new ResponseEntity<Boolean>(true, HttpStatus.OK);
        } catch (RhythModeException e) {
            return new ResponseEntity<Boolean>(false, HttpStatus.OK);
        }
    }
}