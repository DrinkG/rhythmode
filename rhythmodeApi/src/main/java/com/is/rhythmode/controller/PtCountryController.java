package com.is.rhythmode.controller;

import com.is.rhythmode.exception.RhythModeException;
import com.is.rhythmode.model.entity.PtCountry;
import com.is.rhythmode.service.PtCountryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(value = "/country")
public class PtCountryController {
    @Autowired
    private PtCountryService ptCountryService;

    @GetMapping()
    private ResponseEntity<List<PtCountry>> getAllCountries() {
        try {
            List<PtCountry> ptCountryList = ptCountryService.searchCountries();
            return new ResponseEntity<>(ptCountryList, HttpStatus.OK);
        } catch (RhythModeException e) {
            return new ResponseEntity(e.getMessage(), HttpStatus.NOT_FOUND);
        }
    }

    @GetMapping(value = "/{idCountry}")
    private ResponseEntity<PtCountry> getCountryById(@PathVariable Long idCountry) {
        try {
            PtCountry ptCountry = ptCountryService.getById(idCountry);
            return new ResponseEntity<PtCountry>(ptCountry, HttpStatus.OK);
        } catch (RhythModeException e) {
            return new ResponseEntity(null, HttpStatus.NOT_FOUND);
        }
    }

    @GetMapping(value = "/pref/{prefix}")
    private ResponseEntity<PtCountry> getPtTypePrivacity(@PathVariable String prefix) {
        try {
            PtCountry ptCountry = ptCountryService.findByPrefix(prefix);
            return new ResponseEntity<PtCountry>(ptCountry, HttpStatus.OK);
        } catch (RhythModeException e) {
            return new ResponseEntity(null, HttpStatus.NOT_FOUND);
        }
    }

    @DeleteMapping(value = "/delete")
    private ResponseEntity<Boolean> deletePtCountryById(@RequestParam(value="idCountry", required=true) Long idPtCountry) {
        try{
            ptCountryService.deletePtCountry(idPtCountry);
            return new ResponseEntity<Boolean>(true, HttpStatus.OK);
        } catch (RhythModeException e) {
            return new ResponseEntity<Boolean>(false, HttpStatus.OK);
        }
    }
}