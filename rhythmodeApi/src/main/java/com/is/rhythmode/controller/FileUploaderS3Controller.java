package com.is.rhythmode.controller;

import com.is.rhythmode.exception.RhythModeException;
import com.is.rhythmode.model.entity.CommentPost;
import com.is.rhythmode.model.entity.Friend;
import com.is.rhythmode.service.CommentPostService;
import com.is.rhythmode.service.FileUploaderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(value = "/fileUpload")
public class FileUploaderS3Controller{

    private FileUploaderService fileUploaderService;

    @PostMapping(value = "/addFile")
    private ResponseEntity<String> addFile(@RequestParam(value = "fileName", required = true) String fileName,
                                                     @RequestParam(value = "folder", required = true) String folder,
                                                     @RequestParam(value = "fileContent", required = true) byte[] fileContent
    ) {
        try {
            fileUploaderService.uploadData(folder,fileName,fileContent);

            return new ResponseEntity<>("Archivo subido satisfactoriamente", HttpStatus.OK);
        } catch (RhythModeException e) {
            return new ResponseEntity(e.getMessage(), HttpStatus.OK);
        }
    }

    @GetMapping(value = "/getFile")
    private ResponseEntity<byte[]> getFile(@RequestParam(value = "fileName", required = true) String fileName,
                                                 @RequestParam(value = "folder", required = true) String folder
    ) {
        try {
            byte[] arrayByte = fileUploaderService.getData(folder,fileName);

            return new ResponseEntity<>(arrayByte, HttpStatus.OK);
        } catch (RhythModeException e) {
            return new ResponseEntity(e.getMessage(), HttpStatus.OK);
        }
    }
}
