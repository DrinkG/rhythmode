package com.is.rhythmode.controller;

import com.is.rhythmode.exception.RhythModeException;
import com.is.rhythmode.model.entity.Profile;
import com.is.rhythmode.service.ProfileService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Date;
import java.util.List;

@RestController
@RequestMapping(value = "/profile")
public class ProfileController {
    @Autowired
    private ProfileService profileService;

    @GetMapping()
    private ResponseEntity<List<Profile>> getAllProfiles() {
        try {
            List<Profile> commentPostList = profileService.searchProfileList();
            return new ResponseEntity<>(commentPostList, HttpStatus.OK);
        } catch (RhythModeException e) {
            return new ResponseEntity(e.getMessage(), HttpStatus.NOT_FOUND);
        }
    }

    @GetMapping(value = "/{idProfile}")
    private ResponseEntity<Profile> getProfileById(@PathVariable Long idProfile) {
        try {
            Profile commentPost=profileService.getById(idProfile);
            return new ResponseEntity<Profile>(commentPost, HttpStatus.OK);
        } catch (RhythModeException e) {
            return new ResponseEntity(null, HttpStatus.NOT_FOUND);
        }
    }

    @GetMapping(value = "/user/{idUser}")
    private ResponseEntity<Profile> getProfileByIdUser(@PathVariable Long idUser) {
        try {
            Profile commentPost=profileService.getByIdUser(idUser);
            return new ResponseEntity<Profile>(commentPost, HttpStatus.OK);
        } catch (RhythModeException e) {
            return new ResponseEntity(null, HttpStatus.NOT_FOUND);
        }
    }

    @PostMapping(value = "/add")
    private ResponseEntity<String> addProfile(@RequestParam(value = "idUser", required = true) Long idUser,
                                            @RequestParam(value = "imageHeader", required = false) String imageHeader,
                                            @RequestParam(value = "birthday", required = false) Date birthday,
                                            @RequestParam(value = "title", required = false) String title,
                                            @RequestParam(value = "bio", required = false) String bio,
                                            @RequestParam(value = "address", required = false) String address,
                                            @RequestParam(value = "phone", required = false) String phone,
                                            @RequestParam(value = "idTypePrivacy", required = false) Long idTypePrivacy,
                                            @RequestParam(value = "idCountry", required = false) Long idCountry
    ) {
        try {
            if(!profileService.existProfile(idUser)){
                profileService.addProfile(idUser, imageHeader, birthday, title, bio, address, phone, idTypePrivacy, idCountry);
            }else{
                profileService.updateProfile(idUser, imageHeader, birthday, title, bio, address, phone, idTypePrivacy, idCountry);
            }
            return new ResponseEntity<>("Perfil creado satisfactoriamente", HttpStatus.OK);
        } catch (RhythModeException e) {
            return new ResponseEntity(e.getMessage(), HttpStatus.OK);
        }
    }

    @DeleteMapping(value = "/delete")
    private ResponseEntity<Boolean> deleteProfileById(@RequestParam(value="idProfile", required=true) Long idProfile) {
        try{
            profileService.deleteProfile(idProfile);
            return new ResponseEntity<Boolean>(true, HttpStatus.OK);
        } catch (RhythModeException e) {
            return new ResponseEntity<Boolean>(false, HttpStatus.OK);
        }
    }
}