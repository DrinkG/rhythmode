package com.is.rhythmode.controller;

import com.is.rhythmode.exception.RhythModeException;
import com.is.rhythmode.model.entity.Post;
import com.is.rhythmode.service.PostService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Date;
import java.util.List;

@RestController
@RequestMapping(value = "/post")
public class PostController {
    @Autowired
    private PostService postService;

    @GetMapping()
    private ResponseEntity<List<Post>> getAllPost() {
        try {
            List<Post> postList = postService.searchAllPost();
            return new ResponseEntity<>(postList, HttpStatus.OK);
        } catch (RhythModeException e) {
            return new ResponseEntity(e.getMessage(), HttpStatus.NOT_FOUND);
        }
    }

    @GetMapping(value = "/{idPost}")
    private ResponseEntity<Post> getPostById(@PathVariable Long idPost) {
        try {
            Post post=postService.getById(idPost);
            return new ResponseEntity<Post>(post, HttpStatus.OK);
        } catch (RhythModeException e) {
            return new ResponseEntity(null, HttpStatus.NOT_FOUND);
        }
    }

    @GetMapping(value = "/user/{idUser}")
    private ResponseEntity<List<Post>> getPostByIdUser(@PathVariable Long idUser) {
        try {
            List<Post> postList=postService.getPostByIdUser(idUser);
            return new ResponseEntity<>(postList, HttpStatus.OK);
        } catch (RhythModeException e) {
            return new ResponseEntity(e.getMessage(), HttpStatus.NOT_FOUND);
        }
    }

    @PostMapping(value = "/add")
    private ResponseEntity<String> createPost(@RequestParam(value = "idUser", required = true) Long idUser,
                                              @RequestParam(value = "title", required = false) String title,
                                              @RequestParam(value = "location", required = false) String location,
                                              @RequestParam(value = "src", required = true) String src
    ) {
        try {
            postService.createPost(idUser, title, location, src);

            return new ResponseEntity<>("Post creado satisfactoriamente", HttpStatus.OK);
        } catch (RhythModeException e) {
            return new ResponseEntity(e.getMessage(), HttpStatus.OK);
        }
    }

    @DeleteMapping(value = "/delete")
    private ResponseEntity<Boolean> deletePostById(@RequestParam(value="idPost", required=true) Long idPost) {
        try{
            postService.deletePost(idPost);
            return new ResponseEntity<Boolean>(true, HttpStatus.OK);
        } catch (RhythModeException e) {
            return new ResponseEntity<Boolean>(false, HttpStatus.OK);
        }
    }
}