package com.is.rhythmode.controller;

import com.is.rhythmode.exception.RhythModeException;
import com.is.rhythmode.model.entity.UserEntity;
import com.is.rhythmode.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(value = "/user")
public class UserController {
    @Autowired
    private UserService userService;

    @GetMapping()
    private ResponseEntity<List<UserEntity>> getAllUsers() {
        try {
            List<UserEntity> commentPostList = userService.searchUsers();
            return new ResponseEntity<>(commentPostList, HttpStatus.OK);
        } catch (RhythModeException e) {
            return new ResponseEntity(e.getMessage(), HttpStatus.NOT_FOUND);
        }
    }

    @GetMapping(value = "/{idUser}")
    private ResponseEntity<UserEntity> getUserById(@PathVariable Long idUser) {
        try {
            UserEntity commentPost=userService.searchUsuario(idUser);
            return new ResponseEntity<UserEntity>(commentPost, HttpStatus.OK);
        } catch (RhythModeException e) {
            return new ResponseEntity(null, HttpStatus.NOT_FOUND);
        }
    }

    @PostMapping(value = "/register")
    //String email, String password, Integer tlfno, String name, String surname
    private ResponseEntity<String> register(@RequestParam(value = "name", required = true) String name,
                                            @RequestParam(value = "lastName", required = false) String lastName,
                                            @RequestParam(value = "username", required = true) String username,
                                            @RequestParam(value = "email", required = true) String email,
                                            @RequestParam(value = "password", required = true) String password
    ) {
        try {
            userService.registerUser(name, lastName, username, email, password);
            return new ResponseEntity<>("Registro satisfactorio", HttpStatus.OK);
        } catch (RhythModeException e) {
            return new ResponseEntity(e.getMessage(), HttpStatus.OK);
        }
    }

    @DeleteMapping(value = "/delete")
    private ResponseEntity<Boolean> deleteUserById(@RequestParam(value="idUser", required=true) Long idUser) {
        try{
            userService.deleteUser(idUser);
            return new ResponseEntity<Boolean>(true, HttpStatus.OK);
        } catch (RhythModeException e) {
            return new ResponseEntity<Boolean>(false, HttpStatus.OK);
        }
    }
}