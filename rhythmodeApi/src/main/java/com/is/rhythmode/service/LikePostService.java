package com.is.rhythmode.service;

import com.is.rhythmode.model.entity.LikePost;
import com.is.rhythmode.model.entity.Post;
import com.is.rhythmode.model.repository.LikePostRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.is.rhythmode.exception.RhythModeException;

import java.util.Date;
import java.util.List;
import java.util.Optional;

@Service
public class LikePostService {
    @Autowired
    private LikePostRepository likePostRepository;

    public List<LikePost> searchLikePostList() throws RhythModeException {
        List<LikePost> likePostList = likePostRepository.findAll();

        if (likePostList != null) {
            return likePostList;
        } else throw new RhythModeException("No se ha encontrado ningun item");
    }

    public LikePost getById(Long idLikePost) throws RhythModeException {
        Optional<LikePost> likePostOptional = likePostRepository.findById(idLikePost);

        if (likePostOptional.isPresent()) {
            return likePostOptional.get();
        } else throw new RhythModeException("No se ha encontrado el amigo");
    }

    public List<LikePost> searchLikePostByIdPost(Long idPost) throws RhythModeException {
        List<LikePost> friendList = likePostRepository.findByIdPost(idPost);
        return friendList;
    }

    public Long countLikesByIdPost(Long idPost) throws RhythModeException {
        Long likes = likePostRepository.countByIdPost(idPost);
        return likes;
    }

    public void createLikePost(Long idUser,Long  idPost, int liked) throws RhythModeException {
        LikePost likePost = new LikePost();
        likePost.setIdUser(idUser);
        likePost.setIdPost(idPost);
        likePost.setLiked(liked);
        likePost.setCreatedAt(new Date());

        LikePost id = likePostRepository.save(likePost);
        if (id == null)
            throw new RhythModeException("Hubo en error al dar like al post");
    }

    public void deleteLikePost(Long idLikePost) throws RhythModeException {
        try{
            likePostRepository.deleteById(idLikePost);
        }catch(Exception e){
            throw new RhythModeException(e.getMessage());
        }
    }
}
