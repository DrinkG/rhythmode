package com.is.rhythmode.service;

import com.is.rhythmode.model.entity.Friend;
import com.is.rhythmode.model.entity.Post;
import com.is.rhythmode.model.repository.FriendRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.is.rhythmode.exception.RhythModeException;

import java.util.Date;
import java.util.List;
import java.util.Optional;

@Service
public class FriendService {
    @Autowired
    private FriendRepository friendRepository;

    public List<Friend> searchFriends() throws RhythModeException {
        List<Friend> friendList = friendRepository.findAll();

        if (friendList != null) {
            return friendList;
        } else throw new RhythModeException("No se ha encontrado ningun item");
    }

    public Friend getById(Long idFriend) throws RhythModeException {
        Optional<Friend> friendOptional = friendRepository.findById(idFriend);

        if (friendOptional.isPresent()) {
            return friendOptional.get();
        } else throw new RhythModeException("No se ha encontrado el amigo");
    }

    public List<Friend> findFollowFromByUser(Long idUser) throws RhythModeException {
        List<Friend> friendList = friendRepository.findByIdUserFrom(idUser);
        return friendList;
    }

    public List<Friend> findFollowToByUser(Long idUser) throws RhythModeException {
        List<Friend> friendList = friendRepository.findByIdUserTo(idUser);
        return friendList;
    }

    public void deleteFriend(Long idFriend) throws RhythModeException {
        try{
            friendRepository.deleteById(idFriend);
        }catch(Exception e){
            throw new RhythModeException(e.getMessage());
        }
    }

    public Long followerCountByUser(Long idUser) {
        Long followersCount = friendRepository.countByIdUserTo(idUser);
        return followersCount;
    }

    public Long followedCountByUser(Long idUser) {
        Long followedCount = friendRepository.countByIdUserFrom(idUser);
        return followedCount;
    }

    public void addFriend(Long idUserFrom, Long idUserTo) throws RhythModeException {
        Friend friend = new Friend();
        friend.setIdUserFrom(idUserFrom);
        friend.setIdUserTo(idUserTo);
        friend.setCreatedAt(new Date());

        Friend id = friendRepository.save(friend);
        if (id == null)
            throw new RhythModeException("Hubo en error al crear el amigo");
    }
}
