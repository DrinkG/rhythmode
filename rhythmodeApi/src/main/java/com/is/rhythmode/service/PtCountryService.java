package com.is.rhythmode.service;

import com.is.rhythmode.model.entity.PtCountry;
import com.is.rhythmode.model.repository.PtCountryRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.is.rhythmode.exception.RhythModeException;

import java.util.List;
import java.util.Optional;

@Service
public class PtCountryService {
    @Autowired
    private PtCountryRepository ptCountryRepository;

    public List<PtCountry> searchCountries() throws RhythModeException {
        List<PtCountry> ptCountryList = ptCountryRepository.findAll();

        if (ptCountryList != null) {
            return ptCountryList;
        } else throw new RhythModeException("No se ha encontrado ningun item");
    }

    public PtCountry getById(Long idCountry) throws RhythModeException {
        Optional<PtCountry> countryOptional = ptCountryRepository.findById(idCountry);

        if (countryOptional.isPresent()) {
            return countryOptional.get();
        } else throw new RhythModeException("No se ha encontrado el pais");
    }

    public PtCountry findByPrefix(String prefix) throws RhythModeException {
        List<PtCountry> ptCountryList = ptCountryRepository.findByPrefix(prefix);

        if (ptCountryList != null) {
            return ptCountryList.get(0);
        } else throw new RhythModeException("No se ha encontrado el pais");
    }

    public void deletePtCountry(Long idPtCountry) throws RhythModeException {
        try{
            ptCountryRepository.deleteById(idPtCountry);
        }catch(Exception e){
            throw new RhythModeException(e.getMessage());
        }
    }
}
