package com.is.rhythmode.service;

import com.is.rhythmode.exception.RhythModeException;
import com.is.rhythmode.model.entity.Profile;
import com.is.rhythmode.model.entity.UserEntity;
import com.is.rhythmode.model.repository.ProfileRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;
import java.util.Optional;

@Service
public class ProfileService {
    @Autowired
    private ProfileRepository profileRepository;

    public List<Profile> searchProfileList() throws RhythModeException {
        List<Profile> profileList = profileRepository.findAll();

        if (profileList != null) {
            return profileList;
        } else throw new RhythModeException("No se ha encontrado ningun item");
    }

    public Profile getById(Long idProfile) throws RhythModeException {
        Optional<Profile> postOptional = profileRepository.findById(idProfile);

        if (postOptional.isPresent()) {
            return postOptional.get();
        } else throw new RhythModeException("No se ha encontrado ese post");
    }

    public Profile getByIdUser(Long idUser) throws RhythModeException {
        Optional<Profile> profileOptional = profileRepository.findByIdUser(idUser);

        if (profileOptional.isPresent()) {
            return profileOptional.get();
        } else throw new RhythModeException("No se ha encontrado el perfil para ese id de usuario");
    }

    public boolean existProfile(Long idUser){
        Optional<Profile> profileOptional = profileRepository.findByIdUser(idUser);
        if (profileOptional.isPresent()) {
            return true;
        } else {
            return false;
        }
    }

    public void addProfile(Long idUser, String imageHeader, Date birthday, String title, String bio, String address, String phone, Long idTypePrivacy, Long idCountry) throws RhythModeException {
        if (idUser==null || idUser == 0)
            throw new RhythModeException("El id de usuario no puede ser nulo");

        Profile profile = new Profile();
        profile.setIdUser(idUser);
        profile.setImageHeader(imageHeader);
        profile.setBirthday(birthday);
        profile.setTitle(title);
        profile.setBio(bio);
        profile.setAddress(address);
        profile.setPhone(phone);
        profile.setIdTypePrivacy(idTypePrivacy);
        profile.setIdCountry(idCountry);


        Profile id = profileRepository.save(profile);
        if (id == null)
            throw new RhythModeException("Hubo en error al crear el perfil");
    }

    public void updateProfile(Long idUser, String imageHeader, Date birthday, String title, String bio, String address, String phone, Long idTypePrivacy, Long idCountry) throws RhythModeException {
        if (idUser==null || idUser == 0)
            throw new RhythModeException("El id de usuario no puede ser nulo");

        Profile profile = getByIdUser(idUser);

        if(imageHeader!=null && !imageHeader.equals("")){
            profile.setImageHeader(imageHeader);
        }

        if(birthday!=null && !birthday.equals("")){
            profile.setBirthday(birthday);
        }

        if(title!=null && !title.equals("")){
            profile.setTitle(title);
        }

        if(bio!=null && !bio.equals("")){
            profile.setBio(bio);
        }

        if(address!=null && !address.equals("")){
            profile.setAddress(address);
        }

        if(phone!=null && !phone.equals("")){
            profile.setPhone(phone);
        }

        if(idTypePrivacy!=null && idTypePrivacy!=0){
            profile.setIdTypePrivacy(idTypePrivacy);
        }

        if(idCountry!=null && idCountry!=0){
            profile.setIdCountry(idCountry);
        }

        Profile id = profileRepository.save(profile);
        if (id == null)
            throw new RhythModeException("Hubo en error al editar el perfil");
    }

    public void deleteProfile(Long idProfile) throws RhythModeException {
        try{
            profileRepository.deleteById(idProfile);
        }catch(Exception e){
            throw new RhythModeException(e.getMessage());
        }
    }
}
