package com.is.rhythmode.service;

import com.is.rhythmode.model.entity.Post;
import com.is.rhythmode.model.repository.PostRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.is.rhythmode.exception.RhythModeException;

import java.util.Date;
import java.util.List;
import java.util.Optional;

@Service
public class PostService {
    @Autowired
    private PostRepository postRepository;

    public List<Post> searchAllPost() throws RhythModeException {
        List<Post> postList = postRepository.findAll();

        if (postList != null) {
            return postList;
        } else throw new RhythModeException("No se ha encontrado ningun item");
    }

    public Post getById(Long idPost) throws RhythModeException {
        Optional<Post> postOptional = postRepository.findById(idPost);

        if (postOptional.isPresent()) {
            return postOptional.get();
        } else throw new RhythModeException("No se ha encontrado ese post");
    }

    public List<Post> getPostByIdUser(Long idUser) throws RhythModeException {
        List<Post> postListUser = postRepository.findByIdUser(idUser);

        if (postListUser != null) {
            return postListUser;
        } else throw new RhythModeException("No se ha encontrado ningun item para ese usuario");
    }

    public void createPost(Long idUser, String title, String location, String src) throws RhythModeException {
        Post post = new Post();
        post.setTitle(title);
        post.setLocation(location);
        post.setSrc(src);
        post.setIdUser(idUser);
        post.setCreatedAt(new Date());

        Post id = postRepository.save(post);
        if (id == null)
            throw new RhythModeException("Hubo en error al crear el post");
    }

    public void deletePost(Long idPost) throws RhythModeException {
        try{
            postRepository.deleteById(idPost);
        }catch(Exception e){
            throw new RhythModeException(e.getMessage());
        }
    }
}
