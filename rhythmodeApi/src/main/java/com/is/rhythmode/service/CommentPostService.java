package com.is.rhythmode.service;

import com.is.rhythmode.model.entity.CommentPost;
import com.is.rhythmode.model.entity.LikePost;
import com.is.rhythmode.model.repository.CommentPostRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.is.rhythmode.exception.RhythModeException;

import java.util.Date;
import java.util.List;
import java.util.Optional;

@Service
public class CommentPostService {
    @Autowired
    private CommentPostRepository commentPostRepository;
    
    public List<CommentPost> searchCommentPost() throws RhythModeException {
        List<CommentPost> commentPostList = commentPostRepository.findAll();

        if (commentPostList != null) {
            return commentPostList;
        } else throw new RhythModeException("No se ha encontrado ningun item");
    }

    public CommentPost getById(Long commentPostId) throws RhythModeException {
        Optional<CommentPost> commentPostOptional = commentPostRepository.findById(commentPostId);
        
        if (commentPostOptional.isPresent()) {
            return commentPostOptional.get();
        } else throw new RhythModeException("No se ha encontrado el comentario");
    }

    public List<CommentPost> searchCommentsByPost(Long idPost) throws RhythModeException {
        List<CommentPost> commentPostList = commentPostRepository.findByIdPost(idPost);

        if (commentPostList != null) {
            return commentPostList;
        } else throw new RhythModeException("No se ha encontrado ningun item");
    }

    public void createCommentPost(Long idUser, Long idPost, String content, Integer referencesTo) throws RhythModeException {
        CommentPost commentPost = new CommentPost();
        commentPost.setIdUser(idUser);
        commentPost.setIdPost(idPost);
        commentPost.setContent(content);
        commentPost.setReferencesTo(referencesTo);
        commentPost.setCreatedAt(new Date());

        CommentPost id = commentPostRepository.save(commentPost);
        if (id == null)
            throw new RhythModeException("Hubo en error al crear un comentario en el post");
    }

    public void deleteCommentPost(Long idCommentPost) throws RhythModeException {
        try{
            commentPostRepository.deleteById(idCommentPost);
        }catch(Exception e){
            throw new RhythModeException(e.getMessage());
        }
    }
}
