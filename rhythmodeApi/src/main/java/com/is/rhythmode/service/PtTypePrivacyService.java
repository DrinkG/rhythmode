package com.is.rhythmode.service;

import com.is.rhythmode.model.entity.PtTypePrivacy;
import com.is.rhythmode.model.repository.PtTypePrivacyRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.is.rhythmode.exception.RhythModeException;

import java.util.List;
import java.util.Optional;

@Service
public class PtTypePrivacyService {
    @Autowired
    private PtTypePrivacyRepository ptTypePrivacyRepository;

    public List<PtTypePrivacy> searchTypePrivacities() throws RhythModeException {
        List<PtTypePrivacy> ptTypePrivacyList = ptTypePrivacyRepository.findAll();

        if (ptTypePrivacyList != null) {
            return ptTypePrivacyList;
        } else throw new RhythModeException("No se ha encontrado ningun item");
    }

    public PtTypePrivacy getById(Long idTypePrivacity) throws RhythModeException {
        Optional<PtTypePrivacy> ptTypePrivacityOptional = ptTypePrivacyRepository.findById(idTypePrivacity);

        if (ptTypePrivacityOptional.isPresent()) {
            return ptTypePrivacityOptional.get();
        } else throw new RhythModeException("No se ha encontrado el tipo de privacidad");
    }

    public void deletePtTypePrivacity(Long idPtTypePrivacity) throws RhythModeException {
        try{
            ptTypePrivacyRepository.deleteById(idPtTypePrivacity);
        }catch(Exception e){
            throw new RhythModeException(e.getMessage());
        }
    }
}
