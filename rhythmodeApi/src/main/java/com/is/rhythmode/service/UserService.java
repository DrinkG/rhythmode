package com.is.rhythmode.service;

import com.is.rhythmode.exception.RhythModeException;
import com.is.rhythmode.model.entity.UserEntity;
import com.is.rhythmode.model.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;
import java.util.Optional;

@Service
public class UserService {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private PasswordEncoder passwordEncoder;

    public UserEntity searchUsuario(Long id) throws RhythModeException {
        UserEntity user;
        Optional<UserEntity> userOptional = userRepository.findById(id);

        if (userOptional.isPresent()) {
            user = userOptional.get();
        } else throw new RhythModeException("No se ha encontrado el user");

        return user;
    }

    public List<UserEntity> searchUsers() throws RhythModeException {
        List<UserEntity> lstUser = userRepository.findAll();

        if (lstUser != null) {
            return lstUser;
        } else throw new RhythModeException("No se ha encontrado ningun user");
    }

    public UserEntity searchUserByEmail(String email) throws RhythModeException {
        UserEntity user;
        Optional<UserEntity> userOptional = userRepository.findByEmail(email);

        if (userOptional.isPresent()) {
            return userOptional.get();
        }
        throw new RhythModeException("Email not found");
    }

    public void registerUser(String name, String lastName, String username, String email, String password) throws RhythModeException {
        if (email.equals("") || email == null)
            throw new RhythModeException("El email no puede ser nulo");
        if (userRepository.findByEmail(email).isPresent())
            throw new RhythModeException("Ya existe un user con ese email");

        UserEntity user = new UserEntity();
        user.setName(name);
        user.setLastName(lastName);
        user.setUsername(username);
        user.setEmail(email);
        user.setPassword(passwordEncoder.encode(password));
        user.setVerified(false);
        user.setAdmin(false);
        user.setVerifiedCheck(false);
        user.setCreatedAt(new Date());


        UserEntity id = userRepository.save(user);
        if (id == null)
            throw new RhythModeException("Hubo en error al registrar el user");
    }

    public void deleteUser(Long idUser) throws RhythModeException {
        try{
            userRepository.deleteById(idUser);
        }catch(Exception e){
            throw new RhythModeException(e.getMessage());
        }
    }
}