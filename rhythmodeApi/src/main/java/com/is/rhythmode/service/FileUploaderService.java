package com.is.rhythmode.service;

import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;
import com.amazonaws.services.s3.model.GetObjectRequest;
import com.amazonaws.services.s3.model.PutObjectRequest;
import com.amazonaws.services.s3.model.S3Object;
import com.is.rhythmode.exception.RhythModeException;

import java.io.*;

public class FileUploaderService {
    private BasicAWSCredentials awsCreds = new BasicAWSCredentials("", "");
    private AmazonS3 s3Client = AmazonS3ClientBuilder.standard().withRegion("us-west-3").withCredentials(new AWSStaticCredentialsProvider(awsCreds)).build();
    private final String bucketName = "rhythmodestorage";

    public FileUploaderService(){
    }

    public void uploadData(String folderName, String fileName, byte[] myByteArray) throws RhythModeException {
        try (FileOutputStream fos = new FileOutputStream('/' + fileName)) {
            fos.write(myByteArray);

            File file = new File('/' + fileName);
            PutObjectRequest request = new PutObjectRequest(bucketName, folderName + "/" + fileName, file);
            s3Client.putObject(request);
        } catch (FileNotFoundException e) {
            throw new RhythModeException(e.getMessage());
        } catch (IOException e) {
            throw new RhythModeException(e.getMessage());
        }
    }

    public byte[] getData(String folderName, String fileName) throws RhythModeException {
        S3Object fullObject;
        fullObject = s3Client.getObject(new GetObjectRequest(bucketName, folderName + "/" + fileName));

        byte[] byteArray=null;
        try{
            InputStream is = fullObject.getObjectContent();
            byteArray = is.readAllBytes();
            is.close();
        }catch(IOException e) {
            throw new RhythModeException(e.getMessage());
        }
        return  byteArray;
    }
}
