package com.is.rhythmode.model.entity;

import javax.persistence.*;

@Entity
@Table(name = "pt_type_privacy")
public class PtTypePrivacy {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="id_type_privacy")
    private Long idTypePrivacy;

    private String description;

    public PtTypePrivacy(Long idTypePrivacy, String description) {
        this.idTypePrivacy = idTypePrivacy;
        this.description = description;
    }

    public PtTypePrivacy() {
    }

    public Long getIdTypePrivacy() {
        return idTypePrivacy;
    }

    public void setIdTypePrivacy(Long idTypePrivacy) {
        this.idTypePrivacy = idTypePrivacy;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
