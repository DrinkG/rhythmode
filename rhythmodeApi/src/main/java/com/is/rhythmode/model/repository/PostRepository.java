package com.is.rhythmode.model.repository;

import com.is.rhythmode.model.entity.Post;
import com.is.rhythmode.model.entity.PtCountry;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface PostRepository extends JpaRepository<Post, Long> {
    List<Post> findByIdUser(Long idUser);
}