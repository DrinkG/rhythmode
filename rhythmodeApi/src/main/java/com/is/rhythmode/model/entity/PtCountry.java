package com.is.rhythmode.model.entity;
import javax.persistence.*;

@Entity
@Table(name = "pt_country")
public class PtCountry {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="id_country")
    private Long idCountry;

    private String name;

    private String prefix;

    public PtCountry() {
    }

    public PtCountry(Long idCountry, String name, String prefix) {
        this.idCountry = idCountry;
        this.name = name;
        this.prefix = prefix;
    }

    public Long getIdCountry() {
        return idCountry;
    }

    public void setIdCountry(Long idCountry) {
        this.idCountry = idCountry;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPrefix() {
        return prefix;
    }

    public void setPrefix(String prefix) {
        this.prefix = prefix;
    }
}
