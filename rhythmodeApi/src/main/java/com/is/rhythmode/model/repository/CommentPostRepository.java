package com.is.rhythmode.model.repository;

import com.is.rhythmode.model.entity.CommentPost;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CommentPostRepository extends JpaRepository<CommentPost, Long> {
    List<CommentPost> findByIdPost(Long idPost);
}