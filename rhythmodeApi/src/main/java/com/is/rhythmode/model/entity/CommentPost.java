package com.is.rhythmode.model.entity;
import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "comment_post")
public class CommentPost {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="id_comment_post")
    private Long idCommentPost;

    private String content;

    @Column(name="created_at")
    private Date createdAt;

    @Column(name="references_to")
    private Integer referencesTo;

    @Column(name="user_id")
    private Long idUser;

    @Column(name="post_id")
    private Long idPost;

    public CommentPost() {
    }

    public CommentPost(Long idCommentPost, String content, Date createdAt, Integer referencesTo, Long idUser, Long idPost) {
        this.idCommentPost = idCommentPost;
        this.content = content;
        this.createdAt = createdAt;
        this.referencesTo = referencesTo;
        this.idUser = idUser;
        this.idPost = idPost;
    }

    public Long getIdCommentPost() {
        return idCommentPost;
    }

    public void setIdCommentPost(Long idCommentPost) {
        this.idCommentPost = idCommentPost;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public Integer getReferencesTo() {
        return referencesTo;
    }

    public void setReferencesTo(Integer referencesTo) {
        this.referencesTo = referencesTo;
    }

    public Long getIdUser() {
        return idUser;
    }

    public void setIdUser(Long idUser) {
        this.idUser = idUser;
    }

    public Long getIdPost() {
        return idPost;
    }

    public void setIdPost(Long idPost) {
        this.idPost = idPost;
    }
}