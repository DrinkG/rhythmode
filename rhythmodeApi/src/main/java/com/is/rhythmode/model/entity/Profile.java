package com.is.rhythmode.model.entity;
import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "profile")
public class Profile {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="id_profile")
    private Long idProfile;

    @Column(name="image_header")
    private String imageHeader;

    @Column(name="day_of_birth")
    private Date birthday;

    private String title;

    private String bio;

    private String address;

    private String phone;

    @Column(name="type_privacy_id")
    private Long idTypePrivacy;

    @Column(name="user_id")
    private Long idUser;

    @Column(name="country_id")
    private Long idCountry;

    public Profile() {
    }

    public Profile(Long idProfile,String imageHeader, Date birthday, String title, String bio, String address, String phone, Long idTypePrivacy, Long idUser, Long idCountry) {
        this.idProfile=idProfile;
        this.imageHeader = imageHeader;
        this.birthday = birthday;
        this.title = title;
        this.bio = bio;
        this.address = address;
        this.phone = phone;
        this.idTypePrivacy = idTypePrivacy;
        this.idUser = idUser;
        this.idCountry = idCountry;
    }

    public Long getIdProfile() {
        return idProfile;
    }

    public void setIdProfile(Long idProfile) {
        this.idProfile = idProfile;
    }

    public String getImageHeader() {
        return imageHeader;
    }

    public void setImageHeader(String imageHeader) {
        this.imageHeader = imageHeader;
    }

    public Date getBirthday() {
        return birthday;
    }

    public void setBirthday(Date birthday) {
        this.birthday = birthday;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getBio() {
        return bio;
    }

    public void setBio(String bio) {
        this.bio = bio;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public Long getIdTypePrivacy() {
        return idTypePrivacy;
    }

    public void setIdTypePrivacy(Long idTypePrivacy) {
        this.idTypePrivacy = idTypePrivacy;
    }

    public Long getIdUser() {
        return idUser;
    }

    public void setIdUser(Long idUser) {
        this.idUser = idUser;
    }

    public Long getIdCountry() {
        return idCountry;
    }

    public void setIdCountry(Long idCountry) {
        this.idCountry = idCountry;
    }
}