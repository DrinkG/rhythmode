package com.is.rhythmode.model.repository;

import com.is.rhythmode.model.entity.LikePost;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface LikePostRepository extends JpaRepository<LikePost, Long> {
    List<LikePost> findByIdPost(Long idPost);
    Long countByIdPost(Long idPost);
}