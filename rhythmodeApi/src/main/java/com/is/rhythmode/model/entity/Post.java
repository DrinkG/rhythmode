package com.is.rhythmode.model.entity;
import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "post")
public class Post {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="id_post")
    private Long idPost;

    private String title;

    private String location;

    private String src;

    @Column(name="user_id")
    private Long idUser;

    @Column(name="created_at")
    private Date createdAt;

    public Post() {
    }

    public Post(Long idPost, String title, String location, String src, Long idUser, Date createdAt) {
        this.idPost = idPost;
        this.title = title;
        this.location = location;
        this.src = src;
        this.idUser = idUser;
        this.createdAt = createdAt;
    }

    public Long getIdPost() {
        return idPost;
    }

    public void setIdPost(Long idPost) {
        this.idPost = idPost;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getSrc() {
        return src;
    }

    public void setSrc(String src) {
        this.src = src;
    }

    public Long getIdUser() {
        return idUser;
    }

    public void setIdUser(Long idUser) {
        this.idUser = idUser;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }
}