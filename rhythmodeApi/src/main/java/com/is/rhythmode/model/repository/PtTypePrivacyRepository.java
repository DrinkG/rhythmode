package com.is.rhythmode.model.repository;

import com.is.rhythmode.model.entity.PtTypePrivacy;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PtTypePrivacyRepository extends JpaRepository<PtTypePrivacy, Long> {
}