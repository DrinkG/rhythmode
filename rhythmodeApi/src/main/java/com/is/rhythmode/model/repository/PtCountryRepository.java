package com.is.rhythmode.model.repository;

import com.is.rhythmode.model.entity.PtCountry;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface PtCountryRepository extends JpaRepository<PtCountry, Long> {
    List<PtCountry> findByPrefix(String prefix);
}