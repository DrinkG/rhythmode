package com.is.rhythmode.model.entity;
import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "friend")
public class Friend {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="id_friend")
    private Long idFriend;

    @Column(name="user_from_id")
    private Long idUserFrom;

    @Column(name="user_to_id")
    private Long idUserTo;

    @Column(name="created_at")
    private Date createdAt;

    public Friend() {
    }

    public Friend(Long idFriend, Long idUserFrom, Long idUserTo, Date createdAt) {
        this.idFriend = idFriend;
        this.idUserFrom = idUserFrom;
        this.idUserTo = idUserTo;
        this.createdAt = createdAt;
    }

    public Long getIdFriend() {
        return idFriend;
    }

    public void setIdFriend(Long idFriend) {
        this.idFriend = idFriend;
    }

    public Long getIdUserFrom() {
        return idUserFrom;
    }

    public void setIdUserFrom(Long idUserFrom) {
        this.idUserFrom = idUserFrom;
    }

    public Long getIdUserTo() {
        return idUserTo;
    }

    public void setIdUserTo(Long idUserTo) {
        this.idUserTo = idUserTo;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }
}