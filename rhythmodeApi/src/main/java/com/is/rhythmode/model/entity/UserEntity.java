package com.is.rhythmode.model.entity;
import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "user")
public class UserEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="id_user")
    private Long idUser;

    private String name;

    @Column(name="lastname")
    private String lastName;

    private String username;

    private String email;

    private String password;

    private boolean verified;

    @Column(name="is_admin")
    private boolean isAdmin;

    @Column(name="is_verified_check")
    private boolean isVerifiedCheck;

    @Column(name="created_at")
    private Date createdAt;


    public UserEntity() {
    }

    public UserEntity(Long idUser, String name, String lastName, String username, String email, String password, boolean verified, boolean isAdmin, boolean isVerifiedCheck, Date createdAt) {
        this.idUser = idUser;
        this.name = name;
        this.lastName = lastName;
        this.username = username;
        this.email = email;
        this.password = password;
        this.verified = verified;
        this.isAdmin = isAdmin;
        this.isVerifiedCheck = isVerifiedCheck;
        this.createdAt = createdAt;
    }

    public Long getIdUser() {
        return idUser;
    }

    public void setIdUser(Long idUser) {
        this.idUser = idUser;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public boolean isVerified() {
        return verified;
    }

    public void setVerified(boolean verified) {
        this.verified = verified;
    }

    public boolean isAdmin() {
        return isAdmin;
    }

    public void setAdmin(boolean admin) {
        isAdmin = admin;
    }

    public boolean isVerifiedCheck() {
        return isVerifiedCheck;
    }

    public void setVerifiedCheck(boolean verifiedCheck) {
        isVerifiedCheck = verifiedCheck;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }
}
