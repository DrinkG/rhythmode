package com.is.rhythmode.model.entity;
import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "like_post")
public class LikePost {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="id_like_post")
    private Long idLikePost;

    private int liked;

    @Column(name="user_id")
    private Long idUser;

    @Column(name="post_id")
    private Long idPost;

    @Column(name="created_at")
    private Date createdAt;

    public LikePost() {
    }

    public LikePost(Long idLikePost, int liked, Long idUser, Long idPost, Date createdAt) {
        this.idLikePost = idLikePost;
        this.liked = liked;
        this.idUser = idUser;
        this.idPost = idPost;
        this.createdAt = createdAt;
    }

    public Long getIdLikePost() {
        return idLikePost;
    }

    public void setIdLikePost(Long idLikePost) {
        this.idLikePost = idLikePost;
    }

    public int getLiked() {
        return liked;
    }

    public void setLiked(int liked) {
        this.liked = liked;
    }

    public Long getIdUser() {
        return idUser;
    }

    public void setIdUser(Long idUser) {
        this.idUser = idUser;
    }

    public Long getIdPost() {
        return idPost;
    }

    public void setIdPost(Long idPost) {
        this.idPost = idPost;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }
}