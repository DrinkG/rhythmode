package com.is.rhythmode.model.repository;

import com.is.rhythmode.model.entity.Friend;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface FriendRepository extends JpaRepository<Friend, Long> {
    List<Friend> findByIdUserFrom(Long idUser);
    List<Friend> findByIdUserTo(Long idUser);

    Long countByIdUserTo(Long idUserTo);

    Long countByIdUserFrom(Long idUserFrom);

}