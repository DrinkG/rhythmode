package com.is.rhythmode;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class RhythmodeApplication {

	public static void main(String[] args) {
		SpringApplication.run(RhythmodeApplication.class, args);
	}

}
